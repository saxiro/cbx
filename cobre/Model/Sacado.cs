﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace cobre.Model
{
    public class Sacado
    {
        public string NomeSacado { get; set; }
        public string CPFSacado { get; set; }
        public string EnderecoSacado { get; set; }
        public string BairroSacado { get; set; }
        public string CidadeSacado { get; set; }
        public string EstadoSacado { get; set; }
        public string Estado { get; set; }
        public string CEPSacado { get; set; }
        public string CNPJSacado { get; set; }

        public string NumeroDocumento { get; set; }
        public Double ValorDocumento { get; set; }

        public int BancoSacado { get; set; }
        public string AgenciaSacado { get; set; }
        public string ContaCorrenteSacado { get; set; }
    }
}
