﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using cobre.Model;
using Microsoft.AspNetCore.Mvc;

namespace cobre.Controllers
{
    [Produces("application/json")]
    [Route("api/[controller]")]
    [ApiController]
    public class CobreController : ControllerBase
    {
        // GET api/values
        [HttpGet]
        public IActionResult Get()
        {
            Resposta resp = new Resposta() { Descricao = "Estou funcionando!" };

            return Ok(resp);
        }

        //// GET api/values/5
        //[HttpGet("{id}")]
        //public ActionResult<string> Get(int id)
        //{
        //    return "value";
        //}

        // POST api/values
        [HttpPost]
        public IActionResult Post([FromBody]Sacado sacado)
        {
            try
            {
                CobreBemX.ContaCorrente cc = CarregaBanco(sacado.BancoSacado);
                if(cc == null) { return BadRequest("Houve um erro na seleção do Banco."); }

                //Dados do Sacado
                CobreBemX.IDocumentoCobranca _boleto = cc.DocumentosCobranca.Add;

                _boleto.NomeSacado = sacado.NomeSacado;

                if (string.IsNullOrEmpty(sacado.CPFSacado))
                {
                    _boleto.CNPJSacado = sacado.CNPJSacado;
                }
                else
                {
                    _boleto.CPFSacado = sacado.CPFSacado;
                }

                _boleto.EnderecoSacado = sacado.EnderecoSacado;
                _boleto.BairroSacado = sacado.BairroSacado;
                _boleto.CidadeSacado = sacado.CidadeSacado;
                _boleto.EstadoSacado = sacado.EstadoSacado;
                _boleto.CepSacado = sacado.CEPSacado;
                _boleto.NumeroDocumento = sacado.NumeroDocumento; // Voltará o mesmo número no arquivo retorno
                _boleto.ValorDocumento = sacado.ValorDocumento;
                _boleto.AgenciaDebito = sacado.AgenciaSacado; //Sem pontos ou traços
                _boleto.ContaCorrenteDebito = sacado.ContaCorrenteSacado; //Sem pontos ou traços

                //Monta documento de Cobrança
                _boleto.DataDocumento = DateTime.Now.ToShortDateString();
                _boleto.DataVencimento = DateTime.Now.AddDays(3).ToShortDateString();

                _boleto.PadroesBoleto.Demonstrativo = "Referente a pagamentos feitos pelo sistema da Financial Contabilidade</b>";
                _boleto.PadroesBoleto.InstrucoesCaixa = "<br><br>Não cobrar juros ou multa após o vencimento";

                //cc.ImprimeBoletos();
                cc.ArquivoRemessa.Diretorio = @"C:\projetos\cbx\cobre\CobreBemX\Arquivos\Remessa\";
                cc.ArquivoRemessa.DataGravacao = DateTime.Now.ToShortDateString();
                cc.ArquivoRemessa.Layout = "DebitoCC";

                cc.ArquivoRemessa.Arquivo = sacado.NomeSacado + "--" +
                    DateTime.Now.Day + "-" +
                    DateTime.Now.Month + "-" +
                    DateTime.Now.Year + "--" +
                    DateTime.Now.Hour + "-" +
                    DateTime.Now.Minute + "-" +
                    DateTime.Now.Second + "-" +
                    "-REMESSA.TXT";

                cc.GravaArquivoRemessa();
                //cc.EnviaArquivoRemessa();
                
            }
            catch (Exception e)
            {
                Console.WriteLine("Ocorreu um erro: " + e.Message);
                return BadRequest();
            }

            Resposta resp = new Resposta()
            {
                Descricao = "Dados processados!"
            };

            return Ok(resp);
        }

        private static CobreBemX.ContaCorrente CarregaBanco(int banco)
        {
            switch (banco)
            {
                case 1: //Bradesco

                    //Arquivo de licença
                    CobreBemX.ContaCorrente cc = new CobreBemX.ContaCorrenteClass();
                    cc.ArquivoLicenca = @"C:\projetos\cbx\cobre\CobreBemX\001-11.conf";

                    //Configuração do Banco que receberá o dinheiro
                    cc.CodigoAgencia = "1234-5";
                    cc.NumeroContaCorrente = "00000123-X";
                    cc.CodigoCedente = "123456";
                    cc.OutroDadoConfiguracao1 = "019";
                    cc.InicioNossoNumero = "00001";
                    cc.FimNossoNumero = "99999";
                    cc.ProximoNossoNumero = "00015";
                    cc.ArquivoRemessa.Layout = "Febraban"; //Obrigatório para gerar débitos

                    //Logo + Imagens de código de barras
                    cc.PadroesBoleto.PadroesBoletoImpresso.ArquivoLogotipo = @"C:\projetos\cbx\cobre\CobreBemX\logo.jpg";
                    cc.PadroesBoleto.PadroesBoletoImpresso.CaminhoImagensCodigoBarras = @"C:\projetos\cbx\cobre\CobreBemX\Imagens\";
                    return cc;
                default:
                    return cc = null;
            }
        }

        // PUT api/values/5
        [HttpPut("{id}")]
        public void Put(int id, [FromBody] string value)
        {
        }

        // DELETE api/values/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
        }
    }
}
